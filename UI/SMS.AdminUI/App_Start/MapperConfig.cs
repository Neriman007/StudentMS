﻿using AdminUI.Models;
using BLL;

namespace AdminUI.App_Start
{
    public class MapperConfig : AutoMapper.Profile
    {
        public MapperConfig()
        {
            //to view model
            CreateMap<UserViewModel, UserDTO>().ReverseMap();
            CreateMap<UserRoleViewModel, UserRoleDTO>();

            //to dto
            CreateMap<UserRoleDTO, UserRoleViewModel>()
                .ForMember(x => x.UserPermissions, d => d.Ignore());
        }
    }
}