﻿using System.Web.UI.WebControls;
using AdminUI.ServiceFacades;
using AutoMapper;
using BLL.Repositories;
using BLL.Repositories.UserRepositories;
using BLL.Services;
using BLL.Validator;
using DAL.DataContext;
using DAL.Repositories;
using LightInject;

namespace AdminUI.App_Start
{
    public class ServiceConfig
    {
        public static void Register(ServiceContainer serviceContainer)
        {
            Config(serviceContainer);
            RegisterRepositories(serviceContainer);
            RegisterValidators(serviceContainer);
            RegisterServices(serviceContainer);
            RegisterServiceFacades(serviceContainer);
            RegisterMapper();
        }

        public static void Config(ServiceContainer serviceContainer)
        {
            serviceContainer.RegisterControllers();
            serviceContainer.EnableMvc();
            serviceContainer.Register<MainDataContext>(PerContainerLifetime);
        }

        public static void RegisterRepositories(ServiceContainer serviceContainer)
        {
            serviceContainer.Register<IUserRepository, UserRepository>(PerContainerLifetime);
            serviceContainer.Register<IUserRoleRepository, UserRoleRepository>(PerContainerLifetime);
            serviceContainer.Register<IUserPermissionRepository, UserPermissionRepository>(PerContainerLifetime);
        }

        public static void RegisterValidators(ServiceContainer serviceContainer)
        {
            serviceContainer.Register<UserValidator>(PerContainerLifetime);
            serviceContainer.Register<UserRoleValidator>(PerContainerLifetime);
            serviceContainer.Register<UserPermissionValidator>(PerContainerLifetime);
        }

        public static void RegisterServices(ServiceContainer serviceContainer)
        {
            serviceContainer.Register<UserService>(PerContainerLifetime);
            serviceContainer.Register<UserRoleService>(PerContainerLifetime);
            serviceContainer.Register<UserPermissionService>(PerContainerLifetime);
        }

        public static void RegisterServiceFacades(ServiceContainer serviceContainer)
        {
            serviceContainer.Register<UserServiceFacade>(PerContainerLifetime);
            serviceContainer.Register<UserRoleServiceFacade>(PerContainerLifetime);
            serviceContainer.Register<UserPermissionServiceFacade>(PerContainerLifetime);
        }

        private static void RegisterMapper()
        {
            Mapper.Initialize(x =>
            {
                x.AddProfile<DAL.DataContext.MapperConfig>();
                x.AddProfile<AdminUI.App_Start.MapperConfig>();
            });
        }

        private static ILifetime PerContainerLifetime => new PerContainerLifetime();
        private static ILifetime PerRequestLifeTime => new PerRequestLifeTime();
    }
}