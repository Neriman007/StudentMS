﻿using System;
using BLL;
using BLL.Repositories;
using BLL.Services;
using BLL.Validator;
using System.Linq;
using static Common.Constants.Enums;

namespace AdminUI.ServiceFacades
{
    public abstract class BaseServiceFacade<TDto, TRepository, TValidator, TService>
        where TDto : BaseDTO
        where TRepository : IBaseRepository<TDto>
        where TValidator : BaseValidator<TDto>
        where TService : BaseService<TDto, TRepository, TValidator>
    {
        private readonly TService _service;

        public BaseServiceFacade(TService service)
        {
            _service = service;
        }

        public IQueryable<TDto> GetAll(params Status[] statuses)
        {
            var response = _service.GetAll();
            if (response.IsSucceed)
            {
                return response.Data;
            }
            else
            {
                return Enumerable.Empty<TDto>().AsQueryable();
            }
        }

        public TDto GetById(long id, Status status = Status.Active)
        {
            var response = _service.GetAll(status);
            if (response.IsSucceed)
            {
                var dto = response.Data.FirstOrDefault(x => x.Id == id);
                if (dto!=null)
                {
                    return dto;
                }
            }
            return (TDto)Activator.CreateInstance<TDto>();
        }

        public ViewResponse Remove(long id)
        {
            var response = new ViewResponse();
            var removeResult = _service.Remove(id);
            if (!removeResult.IsSucceed)
            {
                response.Failure(removeResult.FailureResult);
            }
            return response;
        }

        public ViewResponse Save(TDto obj)
        {
            var response = new ViewResponse();
            var saveResult = _service.Save(obj);
            if (!saveResult.IsSucceed)
            {
                response.Failure(saveResult.FailureResult);
            }
            return response;
        }
    }
}