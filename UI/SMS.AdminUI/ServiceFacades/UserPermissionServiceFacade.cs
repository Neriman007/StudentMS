﻿using System.Linq;
using BLL;
using BLL.Repositories.UserRepositories;
using BLL.Services;
using BLL.Validator;
using Common.Constants;

namespace AdminUI.ServiceFacades
{
    public class UserPermissionServiceFacade : BaseServiceFacade<UserPermissionDTO,IUserPermissionRepository,UserPermissionValidator,UserPermissionService>
    {
        public UserPermissionServiceFacade(UserPermissionService service) : base(service)
        {
        }
    }
}