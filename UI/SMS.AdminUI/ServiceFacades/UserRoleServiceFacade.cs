﻿using BLL;
using BLL.Repositories.UserRepositories;
using BLL.Services;
using BLL.Validator;

namespace AdminUI.ServiceFacades
{
    public class UserRoleServiceFacade : BaseServiceFacade<UserRoleDTO, IUserRoleRepository, UserRoleValidator, UserRoleService>
    {
        public UserRoleServiceFacade(UserRoleService service) : base(service)
        {
        }
    }
}