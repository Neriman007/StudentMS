﻿using BLL;
using BLL.Services;
using Common.Constants;
using System.Linq;
using BLL.Repositories;
using BLL.Repositories.UserRepositories;
using BLL.Validator;
using DAL.Repositories;

namespace AdminUI.ServiceFacades
{
    public class UserServiceFacade : BaseServiceFacade<UserDTO,IUserRepository,UserValidator,UserService>
    {
        public UserServiceFacade(UserService service) : base(service)
        {
        }
    }
}