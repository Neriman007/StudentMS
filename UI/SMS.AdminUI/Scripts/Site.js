﻿function OnFailure(parameters) {
    alert("Error ocured!");
}

function OnBegin(parameters) {

}

function OnSuccess(response) {
    if (response.IsSucceed) {
        location.href = response.OnSuccessUrl;
    } else {

        new Noty({
            theme: 'limitless',
            layout: 'topRight',
            text: response.Description,
            type: 'error',
            timeout: 2500
        }).show();
    }
}

function HrefMe(url, id) {
    location.href = url + "/" + id;
}

function ShowRemoveModal(url, id) {
    //$('#RemoveModalFormId').attr('Url', url);
    $('#RemoveDataId').val(id);
    $('#RemoveModalId').modal('show');
}

function ChangeLang(lang, url) {
    $.ajax({
        type: 'post',
        url: url,
        data: { lang: lang },
        dataType: 'json',
        success: function(data) {
            location.reload();
            $("#enLang").html(lang);
        },
        error: function() {
            alert('xeta bas verdi!');
        }
    });
}

function RemoveUserPermission(id, url) {
    $('#RemoveDataId').val(id);
    $('#RemoveModalId').modal('show');
}