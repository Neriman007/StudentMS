﻿using BLL;
using static Common.Constants.Enums;

namespace AdminUI.Models
{
    public class UserPermissionViewModel : BaseViewModel
    {
        public long UserRoleId { get; set; }
        public Permission Permission { get; set; }
        public string Description { get; set; }
    }
}