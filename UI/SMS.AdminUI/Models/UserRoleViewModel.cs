﻿using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using Common.Resources;
using DAL;

namespace AdminUI.Models
{
    public class UserRoleViewModel : BaseViewModel
    {
        [Required(ErrorMessageResourceType = typeof(UI), ErrorMessageResourceName = nameof(UI.CannotBeEmpty))]
        public string Name { get; set; }

        [Required(ErrorMessageResourceType = typeof(UI), ErrorMessageResourceName = nameof(UI.CannotBeEmpty))]
        public string Description { get; set; }

        public ICollection<UserPermissionViewModel> UserPermissions { get; set; }
    }
}