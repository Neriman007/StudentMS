﻿using System.ComponentModel.DataAnnotations;
using System.Web;
using Common.Constants;
using Common.Helpers;
using Common.Resources;

namespace AdminUI.Models
{
    public class UserViewModel : BaseViewModel
    {
        [Required(ErrorMessageResourceType = typeof(UI),ErrorMessageResourceName = nameof(UI.CannotBeEmpty))]
        public long UserRoleId { get; set; }

        [Required(ErrorMessageResourceType = typeof(UI), ErrorMessageResourceName = nameof(UI.CannotBeEmpty))]
        public string FirstName { get; set; }

        [Required(ErrorMessageResourceType = typeof(UI), ErrorMessageResourceName = nameof(UI.CannotBeEmpty))]
        public string LastName { get; set; }

        [Required(ErrorMessageResourceType = typeof(UI), ErrorMessageResourceName = nameof(UI.CannotBeEmpty))]
        [Range(1, int.MaxValue, ErrorMessageResourceType = typeof(UI), ErrorMessageResourceName = nameof(UI.CannotBeEmpty))]
        public Enums.Gender Gender { get; set; }

        [Required(ErrorMessageResourceType = typeof(UI), ErrorMessageResourceName = nameof(UI.CannotBeEmpty))]
        [RegularExpression("^[a-zA-Z0-9_\\.-]+@([a-zA-Z0-9-]+\\.)+[a-zA-Z]{2,6}$", ErrorMessageResourceType = typeof(UI), ErrorMessageResourceName = nameof(UI.InvalidEmail))]
        public string Email { get; set; }

        public string Base64StringImage => ConvertHelper.ToBase64String(Image);

        public HttpPostedFileBase PostedImage { get; set; }

        public byte[] Image { get; set; }

        [Required(ErrorMessageResourceType = typeof(UI), ErrorMessageResourceName = nameof(UI.CannotBeEmpty))]
        [RegularExpression("^(?=.*\\d)(?=.*[a-z])(?=.*[A-Z])(?=.*[^\\da-zA-Z])(.{8,15})$", ErrorMessageResourceType = typeof(UI), ErrorMessageResourceName = nameof(UI.WrongPassword))]
        public string Password { get; set; }
    }
}