﻿using Common.Constants;

namespace AdminUI.Models
{
    public abstract class BaseViewModel
    {
        public long Id { get; set; }
        public Enums.Status Status { get; set; }
    }
}