﻿using AdminUI.ServiceFacades;
using BLL;
using DevExtreme.AspNet.Mvc;
using System.Web.Mvc;

namespace AdminUI.Controllers
{
    public class UserPermissionController : BaseController
    {
        private readonly UserPermissionServiceFacade _userPermissionServiceFacade;

        public UserPermissionController(UserPermissionServiceFacade userPermissionServiceFacade)
        {
            _userPermissionServiceFacade = userPermissionServiceFacade;
        }

        public ActionResult Index()
        {
            return View();
        }

        public ActionResult Data(DataSourceLoadOptions loadOptions)
        {
            var data = _userPermissionServiceFacade.GetAll();
            return GetTable<UserPermissionDTO>(data, loadOptions);
        }

        public ActionResult Form(long id)
        {
            return View();
        }

        [HttpPost]
        public ActionResult Save()
        {
            return View();
        }

        [HttpPost]
        public ActionResult Remove()
        {
            return View();
        }
    }
}