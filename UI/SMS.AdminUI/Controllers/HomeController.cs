﻿using BLL.Services;
using System.Linq;
using System.Web.Mvc;
using static Common.Constants.Enums;

namespace AdminUI.Controllers
{
    public class HomeController : Controller
    {
        private readonly UserRoleService _userRoleService;

        public HomeController(UserRoleService userRoleService)
        {
            _userRoleService = userRoleService;
        }
        public ActionResult Index()
        {
            return View();
        }

        public ActionResult About()
        {
            ViewBag.Message = "Your application description page.";

            return View();
        }

        public ActionResult Contact()
        {
            ViewBag.Message = "Your contact page.";

            return View();
        }
    }
}