﻿using System.Globalization;
using System.Threading;
using AdminUI.Models;
using AdminUI.ServiceFacades;
using AutoMapper;
using BLL;
using Common.Constants;
using Common.Helpers;
using DevExtreme.AspNet.Mvc;
using System.Web.Mvc;

namespace AdminUI.Controllers
{
    public class UserController : BaseController
    {
        private readonly UserServiceFacade _userServiceFacade;
        private readonly UserRoleServiceFacade _userRoleServiceFacade;

        public UserController(UserServiceFacade userServiceFacade, UserRoleServiceFacade userRoleServiceFacade)
        {
            _userServiceFacade = userServiceFacade;
            _userRoleServiceFacade = userRoleServiceFacade;
        }
        public ActionResult Index()
        {
            return View();
        }

        public ActionResult Data(DataSourceLoadOptions loadOptions)
        {
            var data = _userServiceFacade.GetAll(Enums.Status.Active);
            return GetTable<UserDTO>(data, loadOptions);
        }

        public ActionResult Form(long id)
        {
            ViewData["Roles"] = _userRoleServiceFacade.GetAll(Enums.Status.Active);
            var dto = _userServiceFacade.GetById(id);
            var viewModel = Mapper.Map<UserViewModel>(dto);
            return View(viewModel);
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Save(UserViewModel userViewModel)
        {
            if (userViewModel.PostedImage != null)
            {
                userViewModel.Image = ConvertHelper.ToBinary(userViewModel.PostedImage);
            }
            var dto = Mapper.Map<UserDTO>(userViewModel);
            var response = _userServiceFacade.Save(dto);
            if (response.IsSucceed)
            {
                response.OnSuccessUrl = Url.Action("Index", "User");
            }
            return Json(response);
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Remove(long id)
        {
            var response = _userServiceFacade.Remove(id);
            if (response.IsSucceed)
            {
                response.OnSuccessUrl = Url.Action("Index", "User");
            }
            return Json(response);
        }
    }
}