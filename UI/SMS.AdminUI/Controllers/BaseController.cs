﻿using BLL.Repositories;
using DevExtreme.AspNet.Data;
using DevExtreme.AspNet.Data.ResponseModel;
using DevExtreme.AspNet.Mvc;
using Newtonsoft.Json;
using System.Globalization;
using System.Linq;
using System.Threading;
using System.Web.Mvc;

namespace AdminUI.Controllers
{
    public class BaseController : Controller
    {
        [HttpPost]
        public ActionResult SetLanguage(string lang)
        {
            if (lang != null)
            {
                Session["Lang"] = lang;
            }

            return Json(new ViewResponse());
        }

        protected override bool DisableAsyncSupport => true;

        protected override void ExecuteCore()
        {
            SetLocalization();
            base.ExecuteCore();
        }

        public void SetLocalization()
        {
            var cultureInfo = new CultureInfo("az-Latn-AZ");
            if (HttpContext.Session["Lang"] != null)
            {
                switch (Session["Lang"].ToString())
                {
                    case "en-US":
                        cultureInfo = new CultureInfo("en-US");
                        break;
                    case "az-Latn-AZ":
                        cultureInfo = new CultureInfo("az-Latn-AZ");
                        break;
                    case "ru-RU":
                        cultureInfo = new CultureInfo("ru-RU");
                        break;
                }
            }
            else
            {
                Session["Lang"] = "az-Latn-AZ";
            }

            Thread.CurrentThread.CurrentCulture = cultureInfo;
            Thread.CurrentThread.CurrentUICulture = cultureInfo;
        }


        public ActionResult GetTable<T>(IQueryable<T> data, DataSourceLoadOptions loadOptions)
        {
            var loadResult = DataSourceLoader.Load<T>(data, loadOptions);
            var serializeData = GetSerializeObject(loadResult);
            return Content(serializeData, "application/json");
        }

        static string GetSerializeObject(LoadResult loadResult)
        {
            var jsonSerializerSetting = new JsonSerializerSettings()
            {
                ReferenceLoopHandling = ReferenceLoopHandling.Ignore
            };
            return JsonConvert.SerializeObject(loadResult, jsonSerializerSetting);
        }
    }
}