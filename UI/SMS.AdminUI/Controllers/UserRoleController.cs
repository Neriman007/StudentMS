﻿using System.Linq;
using AdminUI.Models;
using AdminUI.ServiceFacades;
using AutoMapper;
using BLL;
using Common.Constants;
using DevExtreme.AspNet.Mvc;
using System.Web.Mvc;
using AutoMapper.QueryableExtensions;

namespace AdminUI.Controllers
{
    public class UserRoleController : BaseController
    {
        private readonly UserRoleServiceFacade _userRoleServiceFacade;
        private readonly UserPermissionServiceFacade _userPermissionServiceFacade;

        public UserRoleController(UserRoleServiceFacade userRoleServiceFacade, UserPermissionServiceFacade userPermissionServiceFacade)
        {
            _userRoleServiceFacade = userRoleServiceFacade;
            _userPermissionServiceFacade = userPermissionServiceFacade;
        }
        public ActionResult Index()
        {
            return View();
        }

        public ActionResult Data(DataSourceLoadOptions loadOptions)
        {
            var data = _userRoleServiceFacade.GetAll(Enums.Status.Active);
            return GetTable<UserRoleDTO>(data, loadOptions);
        }

        public ActionResult Form(long id)
        {
            var dto = _userRoleServiceFacade.GetById(id);
            var viewModel = Mapper.Map<UserRoleViewModel>(dto);
            var userPermissions = _userPermissionServiceFacade.GetAll(Enums.Status.Active)
                .Where(x => x.UserRoleId == id).ProjectTo<UserPermissionViewModel>(Mapper.Configuration);
            viewModel.UserPermissions = userPermissions.ToList();
            return View(viewModel);
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Save(UserRoleViewModel userRoleViewModel)
        {
            var dto = Mapper.Map<UserRoleDTO>(userRoleViewModel);
            var response = _userRoleServiceFacade.Save(dto);
            if (response.IsSucceed)
            {
                response.OnSuccessUrl = Url.Action("Index", "UserRole");
            }
            return Json(response);
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Remove(long id)
        {
            var response = _userRoleServiceFacade.Remove(id);
            if (response.IsSucceed)
            {
                response.OnSuccessUrl = Url.Action("Index", "UserRole");
            }
            return Json(response);
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult RemovePermission(long id)
        {
            var response = _userPermissionServiceFacade.Remove(id);
            if (response.IsSucceed)
            {
                response.OnSuccessUrl = Url.Action("Index", "UserRole");
            }
            return Json(response);
        }
    }
}