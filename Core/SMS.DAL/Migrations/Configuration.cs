namespace DAL.Migrations
{
    using Common;
    using Common.Helpers;
    using DAL.DataContext;
    using System;
    using System.Data.Entity.Migrations;
    using System.Linq;
    using static Common.Constants.Enums;

    internal sealed class Configuration : DbMigrationsConfiguration<DAL.DataContext.MainDataContext>
    {
        public Configuration()
        {
            AutomaticMigrationsEnabled = false;
        }

        protected override void Seed(MainDataContext context)
        {
            AddUserRole(context);
            AddUser(context);
            AddUserRolePermissions(context);
        }

        private void AddUser(MainDataContext context)
        {
            var userRole = context.UserRoles.FirstOrDefault(x => x.Name == "AdminGroup");
            if (userRole != null)
            {
                var user = new User
                {
                    CreateDate = DateTime.Now,
                    CreatedUserId = 0,
                    Email = "admin@gmail.com",
                    Image = new byte[0],
                    FirstName = "Neriman",
                    LastName = "Shabanzade",
                    ModifiedUserId = 0,
                    Password = "12345",
                    Status = Status.Active,
                    UserRoleId = userRole.Id,
                    Gender=Gender.Male
                };

                context.Users.AddOrUpdate(x=>x.Email, user);
                context.SaveChanges();
            }
        }

        private void AddUserRole(MainDataContext context)
        {
            var userRole = new UserRole
            {
                CreateDate = DateTime.Now,
                CreatedUserId = 0,
                Description = "Role for super admin",
                ModifiedUserId = 0,
                Name = "AdminGroup",
                Status = Status.Active
            };

            context.UserRoles.AddOrUpdate(x=>x.Name, userRole);
            context.SaveChanges();
        }

        private void AddUserRolePermissions(MainDataContext context)
        {
            var userRole = context.UserRoles.FirstOrDefault(x => x.Name == "AdminGroup");

            foreach (var permission in EnumHelper<Permission>.GetValues())
            {
                var userPermission = new UserPermission
                {
                    CreateDate = DateTime.Now,
                    CreatedUserId = 0,
                    Description = permission.ToString(),
                    ModifiedUserId = 0,
                    Permission = permission,
                    Status = Status.Active,
                    UserRoleId = userRole.Id
                };

                context.UserPermissions.AddOrUpdate(x=>x.Permission, userPermission);
                context.SaveChanges();
            }
        }
    }
}
