﻿using AutoMapper;
using AutoMapper.QueryableExtensions;
using BLL;
using BLL.Repositories;
using Common.Constants;
using Common.Resources;
using Common.Responses;
using System;
using System.Data.Entity;
using System.Linq;

namespace DAL.Repositories
{
    public class BaseRepository<TDao, TDto, TContext> : IBaseRepository<TDto>
        where TDao : BaseEntity
        where TDto : BaseDTO
        where TContext : DbContext
    {
        private readonly TContext _ctx;
        public BaseRepository(TContext ctx)
        {
            _ctx = ctx;
        }

        public ActionResponse<IQueryable<TDto>> GetAll(params Enums.Status[] statuses)
        {
            try
            {
                var data = _ctx.Set<TDao>()
                    .Where(x => !statuses.Any() || statuses.Contains(x.Status))
                    .ProjectTo<TDto>(Mapper.Configuration);
                return ActionResponse<IQueryable<TDto>>.Succeed(data);
            }
            catch (Exception ex)
            {
                return ActionResponse<IQueryable<TDto>>.Failure(ex.Message);
            }
        }

        public ActionResponse Remove(long id)
        {
            try
            {
                var user = _ctx.Set<TDao>().FirstOrDefault(x => x.Id == id);
                if (user == null)
                {
                    return ActionResponse.Failure(UI.UserNotFound);
                }
                else
                {
                    user.Status = Enums.Status.Deleted;
                    _ctx.Entry(user).State = EntityState.Modified;
                    _ctx.SaveChanges();
                    return ActionResponse.Succeed();
                }
            }
            catch (Exception ex)
            {
                return ActionResponse.Failure(ex.Message);
            }
        }

        public ActionResponse Save(TDto obj)
        {
            var response = obj.Id == default(long) ? Add(obj) : Update(obj);
            return response;
        }

        private ActionResponse<TDto> Add(TDto obj)
        {
            try
            {
                var model = Mapper.Map<TDao>(obj);
                model.CreateDate = DateTime.UtcNow.AddHours(4);
                using (var transaction = _ctx.Database.BeginTransaction())
                {
                    _ctx.Set<TDao>().Add(model);
                    var result = _ctx.SaveChanges();
                    transaction.Commit();
                    obj.Id = model.Id;
                    return ActionResponse<TDto>.Succeed(obj);
                }
            }
            catch (Exception ex)
            {
                return ActionResponse<TDto>.Failure(ex.Message);
            }
        }

        private ActionResponse<TDto> Update(TDto obj)
        {
            try
            {
                var model = Mapper.Map<TDao>(obj);
                using (var transaction = _ctx.Database.BeginTransaction())
                {
                    var dbModel = _ctx.Set<TDao>().FirstOrDefault(x => x.Id == model.Id);
                    var entry = _ctx.Entry(dbModel);
                    entry.CurrentValues.SetValues(model);
                    entry.Property(x => x.CreateDate).IsModified = false;
                    _ctx.SaveChanges();
                    transaction.Commit();
                    return ActionResponse<TDto>.Succeed(obj);
                }
            }
            catch (Exception ex)
            {
                return ActionResponse<TDto>.Failure(ex.Message);
            }
        }
    }
}
