﻿using BLL;
using BLL.Repositories.UserRepositories;
using DAL.DataContext;

namespace DAL.Repositories
{
    public class UserPermissionRepository : BaseRepository<UserPermission, UserPermissionDTO, MainDataContext>, IUserPermissionRepository
    {
        public UserPermissionRepository(MainDataContext ctx) : base(ctx)
        {
        }
    }
}
