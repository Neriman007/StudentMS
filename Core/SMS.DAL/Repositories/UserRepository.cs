﻿using BLL;
using BLL.Repositories;
using DAL.DataContext;

namespace DAL.Repositories
{
    public class UserRepository : BaseRepository<User, UserDTO, MainDataContext>, IUserRepository
    {
        public UserRepository(MainDataContext ctx) : base(ctx)
        {
        }
    }
}
