﻿using BLL;
using BLL.Repositories.UserRepositories;
using DAL.DataContext;

namespace DAL.Repositories
{
    public class UserRoleRepository : BaseRepository<UserRole, UserRoleDTO, MainDataContext>, IUserRoleRepository
    {
        public UserRoleRepository(MainDataContext ctx) : base(ctx)
        {
        }
    }
}
