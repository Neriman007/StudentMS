﻿using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace DAL
{
    public class TeacherCourse
    {
        [Key]
        public int TeacherId { get; set; }

        [Key]
        public int CourseId { get; set; }

        [ForeignKey(nameof(TeacherId))]
        public Teacher Teacher { get; set; }

        [ForeignKey(nameof(CourseId))]
        public Course Course { get; set; }
    }
}