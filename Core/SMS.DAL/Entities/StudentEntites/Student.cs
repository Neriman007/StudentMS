﻿using System.ComponentModel.DataAnnotations.Schema;
using System.Security.AccessControl;
using Common.Constants;
using static Common.Constants.Enums;

namespace DAL
{
    public class Student : BaseEntity
    {
        public int TeacherId { get; set; }
        public int CourseId { get; set; }
        public int CalendarId { get; set; }
        public string FirstName { get; set; }
        public string LastName { get; set; }
        public Gender Gender { get; set; }
        public string Email { get; set; }
        public string Password { get; set; }
        public string About { get; set; }
        public long Phone { get; set; }
        public long Address { get; set; }

        [ForeignKey(nameof(TeacherId))]
        public Teacher Teacher { get; set; }

        [ForeignKey(nameof(CourseId))]
        public Course Course { get; set; }

        [ForeignKey(nameof(CalendarId))]
        public Calendar Calendar { get; set; }

    }
}
