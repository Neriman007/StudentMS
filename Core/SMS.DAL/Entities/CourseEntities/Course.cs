﻿namespace DAL
{
    public class Course : BaseEntity
    {
        public string Name { get; set; }
        public string Description { get; set; }
        public decimal Price { get; set; }
        public double DeadLine { get; set; }
        public string Image { get; set; }
        public string Preview { get; set; }
        public int Rating { get; set; }
        public string Content { get; set; }
        public string Review { get; set; }
    }
}