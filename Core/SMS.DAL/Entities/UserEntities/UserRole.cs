﻿namespace DAL
{
    public class UserRole : BaseEntity
    {
        public string Name { get; set; }
        public string Description { get; set; }
    }
}