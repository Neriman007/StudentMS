﻿using System.ComponentModel.DataAnnotations.Schema;
using static Common.Constants.Enums;

namespace DAL
{
    public class User : BaseEntity
    {
        public long UserRoleId { get; set; }
        public string FirstName { get; set; }
        public string LastName { get; set; }
        public Gender Gender { get; set; }
        public string Email { get; set; }
        public byte[] Image { get; set; }
        public string Password { get; set; }

        [ForeignKey(nameof(UserRoleId))]
        public UserRole UserRole { get; set; }
    }
}