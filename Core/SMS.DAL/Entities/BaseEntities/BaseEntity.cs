﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using static Common.Constants.Enums;

namespace DAL
{
    public abstract class BaseEntity
    {
        [Key]
        public long Id { get; set; }
        public DateTime CreateDate { get; set; }
        public Status Status { get; set; }
        public long CreatedUserId { get; set; }
        public long ModifiedUserId { get; set; }
    }
}
