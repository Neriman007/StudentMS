﻿using static Common.Constants.Enums;

namespace DAL
{
    public class Teacher : BaseEntity
    {
        public string FirstName { get; set; }
        public string LastName { get; set; }
        public Gender Gender { get; set; }
        public string Email { get; set; }
        public string Password { get; set; }
        public string About { get; set; }
        public long Phone { get; set; }
        public long Address { get; set; }
        public int Rating { get; set; }
        public string Image { get; set; }
        public string Profession { get; set; }
    }
}