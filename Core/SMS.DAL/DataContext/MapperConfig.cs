﻿using BLL;

namespace DAL.DataContext
{
    public class MapperConfig : AutoMapper.Profile
    {
        public MapperConfig()
        {
            CreateMap<User, UserDTO>();

            CreateMap<UserDTO, User>()
                .ForMember(x => x.UserRole, s => s.Ignore());

            CreateMap<UserRole, UserRoleDTO>();

            CreateMap<UserRoleDTO, UserRole>();

            CreateMap<UserPermission, UserPermissionDTO>();

            CreateMap<UserPermissionDTO, UserPermission>()
                .ForMember(x => x.UserRole, x => x.Ignore());
        }
    }
}