﻿namespace BLL.Repositories.UserRepositories
{
    public interface IUserRoleRepository : IBaseRepository<UserRoleDTO>
    {
    }
}
