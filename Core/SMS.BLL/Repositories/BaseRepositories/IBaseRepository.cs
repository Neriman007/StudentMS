﻿using Common.Responses;
using System.Linq;
using static Common.Constants.Enums;

namespace BLL.Repositories
{
    public interface IBaseRepository<T> where T : BaseDTO
    {
        ActionResponse<IQueryable<T>> GetAll(params Status[] statuses);
        ActionResponse Save(T obj);
        ActionResponse Remove(long id);
    }
}