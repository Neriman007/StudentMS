﻿using BLL.Repositories;
using BLL.Validator;
using Common.Constants;
using Common.Responses;
using System;
using System.Linq;

namespace BLL.Services
{
    public abstract class BaseService<TDto, TRepository, TValidator> : IBaseService<TDto>
        where TDto : BaseDTO
        where TRepository : IBaseRepository<TDto>
        where TValidator : BaseValidator<TDto>
    {
        private readonly TRepository _repository;
        private readonly TValidator _validator;

        public BaseService(TRepository repository, TValidator validator)
        {
            _repository = repository;
            _validator = validator;
        }
        public ActionResponse<IQueryable<TDto>> GetAll(params Enums.Status[] statuses)
        {
            var data = _repository.GetAll(statuses);
            return data;
        }

        public ActionResponse Remove(long id)
        {
            var response = _repository.Remove(id);
            return response;
        }

        public virtual ActionResponse Save(TDto obj)
        {
            try
            {
                var result = _validator.Validate(obj);
                if (result.IsValid)
                {
                    return _repository.Save(obj);
                }
                else
                {
                    return ActionResponse.Failure(result.Errors.Select(x => x.ErrorMessage).ToArray());
                }
            }
            catch (Exception ex)
            {
                return ActionResponse.Failure(ex.Message);
            }
        }
    }
}
