﻿using System;
using System.Linq;
using BLL.Repositories.UserRepositories;
using BLL.Validator;
using Common.Constants;
using Common.Responses;

namespace BLL.Services
{
    public class UserRoleService : BaseService<UserRoleDTO,IUserRoleRepository,UserRoleValidator>
    {
        public UserRoleService(IUserRoleRepository repository, UserRoleValidator validator) : base(repository, validator)
        {
        }
    }
}