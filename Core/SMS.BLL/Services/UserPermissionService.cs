﻿using System;
using BLL.Repositories.UserRepositories;
using Common.Constants;
using Common.Responses;
using System.Linq;
using BLL.Validator;

namespace BLL.Services
{
    public class UserPermissionService : BaseService<UserPermissionDTO,IUserPermissionRepository,UserPermissionValidator>
    {
        public UserPermissionService(IUserPermissionRepository repository, UserPermissionValidator validator) : base(repository, validator)
        {
        }
    }
}
