﻿using BLL.Repositories;
using BLL.Validator;
using Common.Resources;
using Common.Responses;
using System;
using System.Linq;
using static Common.Constants.Enums;

namespace BLL.Services
{
    public class UserService : BaseService<UserDTO, IUserRepository, UserValidator>
    {
        private readonly IUserRepository _userRepository;
        private readonly UserValidator _userValidator;

        public UserService(IUserRepository repository, UserValidator validator) : base(repository, validator)
        {
            _userRepository = repository;
            _userValidator = validator;
        }

        public ActionResponse<UserDTO> SignIn(string email, string password)
        {
            try
            {
                var users = _userRepository.GetAll(Status.Active);
                if (users.IsSucceed)
                {
                    var user = users.Data.FirstOrDefault(x => x.Email == email);
                    if (user == null)
                    {
                        return ActionResponse<UserDTO>.Failure(UI.UserNotFound);
                    }
                    var verify = user.Password == password;
                    if (!verify)
                    {
                        return ActionResponse<UserDTO>.Failure(UI.UserPasswordInCorrect);
                    }
                    return ActionResponse<UserDTO>.Succeed(user);
                }
                else
                {
                    return ActionResponse<UserDTO>.Failure(users.FailureResult);
                }
            }
            catch (Exception ex)
            {
                return ActionResponse<UserDTO>.Failure(ex.Message);
            }
        }

        public override ActionResponse Save(UserDTO obj)
        {
            var users = _userRepository.GetAll(Status.Active);
            if (!users.IsSucceed)
            {
                return ActionResponse.Failure(users.FailureResult);
            }
            var existUser = users.Data.FirstOrDefault(x => x.Email == obj.Email);
            if (existUser != null && existUser.Id!=obj.Id)
            {
                return ActionResponse.Failure("User already exists!");
            }

            return base.Save(obj);
        }
    }
}