﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using FluentValidation;

namespace BLL.Validator
{
    public class BaseValidator<TDto> : AbstractValidator<TDto> 
        where TDto:BaseDTO
    {
    }
}
