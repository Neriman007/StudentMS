﻿using FluentValidation;

namespace BLL.Validator
{
    public class UserRoleValidator : BaseValidator<UserRoleDTO>
    {
        public UserRoleValidator()
        {
            RuleFor(x => x.Name).NotNull().NotEmpty();
            RuleFor(x => x.Description).NotNull().NotEmpty();
        }
    }
}
