﻿using FluentValidation;

namespace BLL.Validator
{
    public class UserValidator : BaseValidator<UserDTO>
    {
        public UserValidator()
        {
            RuleFor(x => x.FirstName).NotNull().NotEmpty();
            RuleFor(x => x.LastName).NotNull().NotEmpty();
            RuleFor(x => x.UserRoleId).GreaterThan(0);
        }
    }
}