﻿using BLL;
using static Common.Constants.Enums;

namespace BLL
{
    public class UserDTO : BaseDTO
    {
        public byte[] Image { get; set; }
        public long UserRoleId { get; set; }
        public string FirstName { get; set; }
        public string LastName { get; set; }
        public Gender Gender { get; set; }
        public string Email { get; set; }
        public string Password { get; set; }
        public UserRoleDTO UserRole { get; set; }
        public string GenderName => Gender.ToString();
    }
}
