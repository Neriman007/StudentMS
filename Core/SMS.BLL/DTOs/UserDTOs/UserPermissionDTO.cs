﻿using Common.Constants;

namespace BLL
{
    public class UserPermissionDTO : BaseDTO
    {
        public long UserRoleId { get; set; }
        public Enums.Permission Permission { get; set; }
        public string Description { get; set; }
        public virtual UserRoleDTO UserRole { get; set; }
    }
}