﻿using System;
using static Common.Constants.Enums;

namespace BLL
{
    public abstract class BaseDTO
    {
        public BaseDTO()
        {
            Status = Status.Active;
        }
        public long Id { get; set; }
        public DateTime CreateDate { get; set; }
        public Status Status { get; set; }
        public long CreatedUserId { get; set; }
        public long ModifiedUserId { get; set; }
        public string StatusName => Status.ToString();
    }
}
