﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Common.Helpers
{
    public class EnumHelper<T> where T: IConvertible
    {
        public static IList<T> GetValues()
        {
            var values = Enum.GetValues(typeof(T)).Cast<T>().ToList<T>();
            return values;
        }
    }
}
