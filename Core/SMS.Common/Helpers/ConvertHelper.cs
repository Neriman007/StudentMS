﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Web;

namespace Common.Helpers
{
    public static class ConvertHelper
    {
        public static byte[] ToBinary(HttpPostedFileBase httpPostedFileBase)
        {
            MemoryStream target=new MemoryStream();
            httpPostedFileBase.InputStream.CopyTo(target);
            byte[] data = target.ToArray();
            return data;
        }

        public static string ToBase64String(byte[] data)
        {
            var base64 = Convert.ToBase64String(data);
            var imgSrc = String.Format("data:image/gif;base64,{0}", base64);
            return imgSrc;
        }
    }
}