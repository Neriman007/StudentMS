﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Common.Resources;

namespace BLL.Repositories
{
    public class ViewResponse
    {
        public ViewResponse()
        {
            IsSucceed = true;
            Description = UI.SuccessResponse;
        }

        public bool IsSucceed { get; set; }
        public string Description { get; set; }
        public string OnSuccessUrl { get; set; }

        public void Failure(string[] failureResult)
        {
            IsSucceed = false;
            Description = string.Join(", ", failureResult);
        }

        public void Failure(string failureResult)
        {
            IsSucceed = false;
            Description = failureResult;
        }
    }
}