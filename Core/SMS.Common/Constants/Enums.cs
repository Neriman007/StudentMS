﻿using Common.Attributes;
using Common.Resources;

namespace Common.Constants
{
    public class Enums
    {
        public enum Status
        {
            None = 0,
            Active = 1,
            Deleted = 10

        }

        public enum Permission
        {
            Base,
            Common,
            StudentManager
        }

        public enum Gender
        {   
            [Localization(nameof(UI.Male),typeof(UI))]
            Male=1,
            [Localization(nameof(UI.Female), typeof(UI))]
            Female =2
        }
    }
}